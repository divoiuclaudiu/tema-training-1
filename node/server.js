const express = require("express");
const mysql = require("mysql");
const app = express(); 

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

let port = 8080; 

app.listen(port, () => { 
    console.log("Serverul merge pe portul " + port);
});

const connection = mysql.createConnection({ 
    host: "localhost",
    user: "root",
    password: "",
    database: "training_crocos"
});

connection.connect((err) => { 
    if (err) throw err; 

    console.log("Baza de date conectata"); 

   const sqlQuery = "CREATE TABLE IF NOT EXISTS Crocodili(id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT, nume VARCHAR(30), prenume VARCHAR(30), telefon VARCHAR(10), email VARCHAR(30), activ BOOLEAN)"; 

   connection.query(sqlQuery, (err) => {
       if (err) throw err;
       console.log ("Tabela crocodili creata");
   });
});


app.post("/crocodili", (req, res) => {
    const croco = {
        nume: req.body.nume,
        prenume: req.body.prenume,
        telefon: req.body.telefon,
        email: req.body.email,
        activ: req.body.activ
    }
    let errors = [];
    if(!croco.nume || !croco.prenume || !croco.telefon || !croco.email || croco.activ === undefined) {
        errors.push ("Nu ai completat toate campurile");
    }
    if (croco.email.includes("@gmail.com") && croco.email.includes("@yahoo.com")) {
        errors.push("Email invalid!");
    }
    if (!croco.telefon.length === 10) {
        errors.push ("Telefon invalid");
    }

    if(errors.length === 0) {
        try {
            const insertQuery = `INSERT INTO Crocodili(nume, prenume, telefon, email, activ) VALUES ('${croco.nume}', '${croco.prenume}', '${croco.telefon}', '${croco.email}', '${croco.activ}')`;

            connection.query (insertQuery, err => {
                if (err) throw err;
                else {
                    console.log("Crocodil inserat");
                    res.status(200).send({message: "Crocodil inserat"});
                }
            })
        } catch (err) {
            console.log ("Server error");
            res.status(500).send(err);
        }
    } else {
        console.log ("Eroare");
        res.status (400).send(errors);
    }

});

app.get ("/crocodili", (req, res) => {
    try {
        let select = "";
        if (req.query.activ)  {
            select = `SELECT * FROM Crocodili WHERE activ = '${req.query.activ}'`;
        }else {
            select = "SELECT * FROM Crocodili";
        }
        connection.query(select, (err, result) => {
            if (err) throw err;
            res.status(200).send(result);
        });
    } catch {
        console.log ("Server error");
        res.status(500).send(err);
    }
});

app.delete("/crocodili/:id", (req,res) => {
    try {
        const sqlDelete = `DELETE FROM Crocodili WHERE id = '${req.params.id}'`;
        connection.query(sqlDelete, (err) => {
            if (err) throw err;
            res.status(200).send({message: "Crocodil disparut"});
        });
    } catch {
        console.log ("Server error");
        res.status(500).send(err);
    }
});

//Tema 
//Get by Id
app.get ("/crocodili/:id", (req, res) => {
    try {
        let selectById = "";
        if (req.params.id)  {
            selectById = `SELECT * FROM Crocodili WHERE id = '${req.params.id}'`;
        }
        connection.query(selectById, (err, result) => {
            if (err) throw err;
            res.status(200).send(result);
        });
    } catch {
        console.log ("Server error");
        res.status(500).send(err);
    }
});

//Put by Id
app.put("/crocodili/:id", (req, res) => {
    const croco = {
        nume: req.body.nume,
        prenume: req.body.prenume,
        telefon: req.body.telefon,
        email: req.body.email,
        activ: req.body.activ
    }
    let errors = [];
    if(!croco.nume || !croco.prenume || !croco.telefon || !croco.email || croco.activ === undefined) {
        errors.push ("Nu ai completat toate campurile");
    }
    if (croco.email.includes("@gmail.com") && croco.email.includes("@yahoo.com")) {
        errors.push("Email invalid!");
    }
    if (!croco.telefon.length === 10) {
        errors.push ("Telefon invalid");
    }

    if(errors.length === 0) {
        try {
            const putQuery = `UPDATE Crocodili SET nume= '${croco.nume}',prenume= '${croco.prenume}',telefon= '${croco.telefon}',email= '${croco.email}',activ= '${croco.activ}' WHERE id = '${req.params.id}'`;

            connection.query (putQuery, err => {
                if (err) throw err;
                else {
                    console.log("Crocodil actualizat");
                    res.status(200).send({message: "Crocodil actualizat"});
                }
            })
        } catch (err) {
            console.log ("Server error");
            res.status(500).send(err);
        }
    } else {
        console.log ("Eroare");
        res.status (400).send(errors);
    }

});