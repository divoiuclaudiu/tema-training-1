//Tema Training

//1
let rezervari = {
    numarRezervariTotale: 3,
    numePersoane: ["Croco", "Tania", "Adrian"],
    nrPersoaneLaMasa: [6, 2, 3]
};
console.log(rezervari);

let rezerva = (nume, nr_pers) => {
    rezervari.numarRezervariTotale++;
    rezervari.numePersoane.push(nume);
    rezervari.nrPersoaneLaMasa.push(nr_pers);
};
rezerva("Mihnea", 10);
console.log (rezervari);

//2
let locuri_restaurant = () => {
    let suma = 0;
    let vector_ocupatie = [];
    for (let i = 0; i < rezervari.nrPersoaneLaMasa.length; i++) {
        suma +=rezervari.nrPersoaneLaMasa[i];
        vector_ocupatie.push(rezervari.numePersoane[i] + "-" + rezervari.nrPersoaneLaMasa[i]);
    }
    console.log (suma);
    console.log (vector_ocupatie);
}
locuri_restaurant();